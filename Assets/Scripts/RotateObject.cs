﻿using UnityEngine;

/// <summary>
/// This script rotates an object.
/// </summary>
public class RotateObject : MonoBehaviour
{
    void Update()
    {
        transform.localEulerAngles = new Vector3(Mathf.Sin(Time.time) * 20, Mathf.Cos(Time.time) * 20, 0);
    }
}
