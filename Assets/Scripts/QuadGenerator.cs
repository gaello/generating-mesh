﻿using UnityEngine;

/// <summary>
/// Script that generates a quad with width and height.
/// </summary>
public class QuadGenerator : MonoBehaviour
{
    // Reference to the mesh filter.
    private MeshFilter meshFilter;

    // Width of our quad.
    [SerializeField]
    private float width = 2;

    // Height of our quad.
    [SerializeField]
    private float height = 2;

    /// <summary>
    /// Unity method called on first frame.
    /// </summary>
    void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        GenerateQuad();
    }

    /// <summary>
    /// Method which generates quad.
    /// </summary>
    private void GenerateQuad()
    {
        // Creating mesh object.
        Mesh mesh = new Mesh();

        // Defining vertices.
        Vector3[] vertices = new Vector3[4]
        {
            new Vector3(-width/2, height/2,0),
            new Vector3(width/2, height/2,0),
            new Vector3(-width/2, -height/2,0),
            new Vector3(width/2, -height/2,0)
        };

        // Defining triangles.
        int[] triangles = new int[6]
        {
            0,1,2, // first triangle
            1,3,2 // second triangle
        };

        // Defining UV.
        Vector2[] uv = new Vector2[4]
        {
            new Vector2(0,1),
            new Vector2(1,1),
            new Vector2(0,0),
            new Vector2(1,0)
        };

        // Assigning vertices, triangles and UV to the mesh.
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;

        // Assigning mesh to mesh filter to display it.
        meshFilter.mesh = mesh;
    }
}
